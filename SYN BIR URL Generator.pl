######################################################################
# Script Name : SYN BIR URL Generator.pl
# Author : Kevin Novini
# Date : 30/09/2020
# Purpose :
#   This script reads a file of BIR report input Controls HTTP requests
#   and calls the BIR webservice to retrieve the list of input controls
#   in XML format. The relevant elements are extracted from the XML
#   and a new HTTP request is generated which can be used to run the 
#   actual report.
######################################################################

use strict;
use warnings;
use v5.10;
use LWP::UserAgent;
use HTTP::Request::Common;
use XML::LibXML;

my $output = "";
 
my $ua = LWP::UserAgent->new();

# Read Input
my $in_filename = 'C:\Kittyhawk\SYN BIR\data.txt';
open(my $ifh, '<:encoding(UTF-8)', $in_filename)
  or die "Could not open file '$in_filename' $!";

# Write Output
my $out_filename = 'C:\Kittyhawk\SYN BIR\report.txt';
open(my $ofh, '>', $out_filename) or die "Could not open file '$out_filename' $!";

# Write Errors
my $err_filename = 'C:\Kittyhawk\SYN BIR\errors.txt';
open(my $efh, '>', $err_filename) or die "Could not open file '$err_filename' $!";
 
while (my $row = <$ifh>) {
  chomp $row;
  print "$row\n";
  process_data($row);
}

sub process_data {

    my $url=$_[0];

    my $request = GET $url;
    
    $request->authorization_basic('webservice', 'PfGEFuYgHeNp3pzRDWh4Xj');
    
    my $response = $ua->request($request);

    # Check for any errors
    print "HTTP status: ", $response->code( ), "\n";
    my $code = $response->code( );
    if ($code ne 200) {
        print $efh "$url " . "(response code = $code)\n";
        return;
    }

    my $dom = XML::LibXML->load_xml(string => $response->content());

    foreach my $inputControl ($dom->findnodes('//inputControl')) {
        say 'id:    ', $inputControl->findvalue('./state/id');
        my $id = $inputControl->findvalue('./state/id');
        say 'value:    ', $inputControl->findvalue('.//value');
        my $first_val = $inputControl->findvalue("(.//value)[1]");
        print "first : $first_val\n";
        say 'visible: ', $inputControl->findvalue('./visible');
        say "";
        my $visible = $inputControl->findvalue('./visible');
        if ($visible eq "true") {
            $output = $output . "$id=$first_val&";
        }
    }

    $output =~ s/&$//;
    $output =~ s/~NULL~//g;
    $output =~ s/~NOTHING~//g;
    $output =~ s/ /%20/g;

    $url =~ s/\/inputControls\//.html?/;

    print $url . $output;
    print $ofh "$url" . "$output\n";
    $output = "";

}

close $ofh;
close $efh;

exit 0;